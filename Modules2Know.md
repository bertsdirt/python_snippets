This list is my base list of Python mods to know.

os (os.path)
sys
re
datetime
time
subprocess
logging

collections
itertools
pickle

math
random
numpy

sqlite3

json
request
flask
tkinter

unittest
