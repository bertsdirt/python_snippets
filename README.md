#Read in 2 command line args, print them back.
```
./my_argparse_example.py foobar tarfu
```

#Read in -f/--file option as arg, print it back.
```
./my_optparse_example.py -f foobar.txt 
./my_optparse_example.py --file=foobar.txt
```

#Show how to use dictionary.
```
./my_dictionary_example.py
```

#simple_RSS.py -- Create a graph (in svg) of today's ResUsage CPU/WIO/MBS.
##Save off a simple_RSS.json and simple_RSS.svg
```
./simple_RSS.py
```
