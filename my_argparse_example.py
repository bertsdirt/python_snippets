#!/usr/bin/env python

import argparse


parser = argparse.ArgumentParser(description='Example of argparse.')
parser.add_argument('myarg1', help='an integer for the accumulator')
parser.add_argument('myarg2')

argv = vars(parser.parse_args())
arg1 = argv['myarg1']
print ('Your 1st arguement is: %s' % arg1)
arg2 = argv['myarg2']
print ('Your 2nd arguement is: %s' % arg2)
