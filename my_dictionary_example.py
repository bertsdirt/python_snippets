#!/usr/bin/env python

def replace_all(text, dic):
    for i, j in dic.iteritems():
        text = text.replace(i, j)
    return text

parms=[':P1',':P2',':P3']
vals=['420','2112','foo_bar']
dictionary=dict(zip(parms,vals))
print (dictionary)

mystr='The time is :P1 in the year :P2 \n\
 and we are :P3'

print (mystr)

mynewstr=replace_all(mystr,dictionary)

print (mynewstr)

