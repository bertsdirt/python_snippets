#!/usr/bin/env python

from optparse import OptionParser


parser = OptionParser()
parser.add_option("-f", "--file", dest="infilename",
                  help="Your input filename")
parser.add_option("-q", "--quiet",
                  action="store_false", dest="verbose", default=True,
                  help="don't print status messages to stdout")

(options, args) = parser.parse_args()

print ('You are going to read in this file: %s' % options.infilename)
