#!/usr/bin/env python

import random 
import argparse
import time
import sys
"""Create random number of . based on input of min and max number to create
   every 2 seconds.
"""

parser = argparse.ArgumentParser()
parser.add_argument('mymin')
parser.add_argument('mymax')

argv = vars(parser.parse_args())
arg1 = int(argv['mymin'])
#print ('Your 1st arguement is: %s' % arg1)
arg2 = int(argv['mymax'])
#print ('Your 2nd arguement is: %s' % arg2)

#For the next 4 hours
count=0
while (count < 3600):
    count = count + 1
    mydots = (random.randint(arg1,arg2))
    z = ('.' * mydots)
    sys.stdout.write(z)
    sys.stdout.flush()
    time.sleep(4)
