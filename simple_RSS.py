#!/usr/bin/env python

import teradata
import json, collections
import requests
import pandas as pd
import pygal

''' This little script will connect to your Teradata system (once you plug in some valid
    values for the variables below) and save a graph of today's Resusage CPU, WaitIO, and
    MB/Sec.  If you want to post the data to Firebase, uncomment the call to the postit 
    function and put in some values for the project and authkey.  An easy way to view
    output SVG file is just to run:  python -m SimpleHTTPServer 8080 --or-- just drag the
    simple_RSS.svg file to your browser.

    For using the Teradata Python module, make sure to setup an ODBCINI file, set the ODBCINI
    and LD_LIBRARY_PATH environment variables 
        ex: LD_LIBRARY_PATH=/opt/teradata/client/16.00/lib64
            ODBCINI=/home/me/.odbcini
    You probably need to adjust the location of the Library path and ODBCINI file.
'''

sysx = 'my_Teradata_system' 
logon_user = 'my_username'
logon_password = 'my_password'
project = 'my_firebase_project'
authkey = 'my_firebase_project_apikey'

def postit(payload):
    url = ('https://%s.firebaseio.com/SimpleRSS.json?auth=%s' % (project, authkey))
    r = requests.post(url, data=(payload))
    print r.status_code

uda_exec = teradata.UdaExec(odbcLibPath='/opt/teradata/client/16.00/odbc_64/lib/libodbc.so',appName='simple_RSS', version='1.0', logConsole=True)
conn = uda_exec.connect(method='odbc',system=sysx,username=logon_user,password=logon_password)

SQL2Execute = "SELECT TheDate ,TheTime /* CPU usr+exec  % */ ,sum((CPUUServ+CPUUExec)/NULLIFZERO(NCPUs))/sum(secs)  (format 'ZZ9', title 'CPU') /* Wait IO % */ ,sum(CPUIoWait/NULLIFZERO(NCPUs))/sum(secs)  (format 'ZZ9', title 'WIO') /*  Physical (Rd + Wr)/sec */ ,sum(FileAcqReadKB+FilePreReadKB+FileWriteKB)/Sum(secs)/1024.0  (format 'ZZZ9.9', title 'MBS') FROM dbc.ResUsageSpma WHERE ( ( TheDate = date AND TheTime >= '00:00:00' ) OR ( TheDate > date )) AND ( ( TheDate = date   AND TheTime <= '23:59:59'   ) OR ( TheDate < date )) GROUP BY TheDate,  TheTime ORDER BY TheDate,  TheTime ; "

stats = []
for row in conn.execute(SQL2Execute):
    d = collections.OrderedDict()
    xdate = str(row.TheDate)
    xtime = str(row.TheTime).zfill(6)
    xdttm=''.join([xdate, xtime])
    d['dttm'] = str(pd.to_datetime(xdttm, format='%Y-%m-%d%H%M%S'))
    d['cpu'] = round(row.CPU,2)
    d['wio'] = round(row.WIO,2)
    d['mbs'] = round(row.MBS,2)
    stats.append(d)

j = json.dumps(stats)

with open ('simple_RSS.json', 'w') as f:
    f.write(j)

print(j)
#postit(j)
line_chart = pygal.StackedLine(fill=True)
line_chart.title = 'Simple RSS'
line_chart.x_labels = ( [ item['dttm'] for item in stats ])
line_chart.add('cpu', [ item['cpu'] for item in stats ])
line_chart.add('wio', [ item['wio'] for item in stats ])
line_chart.add('mbs', [ item['mbs'] for item in stats ], secondary=True)
line_chart.render_to_file('./simple_RSS.svg')


