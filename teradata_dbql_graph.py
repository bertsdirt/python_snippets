#!/usr/bin/env python
import teradata
import pygal
import re

def get_dbql():
    dbql_qry = ("""select top 10
    /* Get last 24 hours queries and graph it*/
    UserName
    ,StartTime
    ,(EXTRACT(HOUR FROM ElapsedTime) * 3600 +  EXTRACT(MINUTE FROM ElapsedTime) + EXTRACT(SECOND FROM ElapsedTime))  (DECIMAL(15,2)) as ElapsedSecs
    ,AMPCPUTime As CPUTime
    ,TotalIOCount (BIGINT) As IOCount
    ,substring (QueryText from 0 for 20) as QryText
    FROM
    DBC.QryLog
    WHERE 
        statementtype = 'select'
        and StartTime >= current_timestamp - interval '1' day
    ORDER BY StartTime; """)
    udaExec = teradata.UdaExec (appName='HelloWorld', version='1.0',logConsole=False)
    session = udaExec.connect(method='odbc', system='dbc', username='dbc' , password='dbc')
    cursor = session.cursor()
    cursor.execute(dbql_qry)
    results = cursor.fetchall()
    foo = []
    for row in session.execute(dbql_qry):
       # print (row[2],row[3],row[4],row[5])
        #print ("%s,%s"  %(row["CPUTime"] , row["IOCount"]))
        bar = ("%s,%s,%s,%s"  %(row["QryText"],row["ElapsedSecs"],row["CPUTime"] , row["IOCount"]))
        foo.append(bar)
    #foo = []
    #for i in results:
    #     foo.append(i)
    #print foo
    #print (foo)
    return  foo

def parse_dbql(dbql_data):
    qtxt = []
    qet = []
    qcpu = []
    qio = []
    for line in dbql_data:
        sline = (line.split(","))
        #print (sline[0])
        #et = re.compile('Duration: (.*?) seconds')
        #ets = et.search(line)
        #qry = re.compile('Query: \/\*(.*?)\*\/ [s,w]')
        #query=qry.search(line)
        #if query:
        qtxt.append(sline[0])
        #qet.append(float(sline[1]))
        qcpu.append(float(sline[2]))
        qio.append(float(sline[3]))
    bar_chart.x_labels = (qtxt)
    #bar_chart.add('Elapsed Time',qet)
    bar_chart.add('AMP CPU',qcpu)
    bar_chart.add('IO Count',qio, secondary=True)


bar_chart=pygal.Bar()
bar_chart.title = ("Last 24Hrs, Top 10")
#logfiles2parse = ['' , '' , '']

#data2parse = get_dbql()
#for data in data2parse:
#    parsefile(logfile2parse)
foo = get_dbql()
parse_dbql(foo)
print (foo)

bar_chart.render_to_file('dbql_last24hrs.svg')

